import {useEffect, useState} from 'react';
import InputsList from './InputsList';

export default function GeneratePresentation(props) {
  const [inputs, setInputs] = useState([]);
  const [updateLocation, setUpdateLocation] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (inputs.length === 0) {
      fetch(`${process.env.REACT_APP_MATIK_BASE_URL}/api/1.0/templates/${props.template.id}/inputs`, {
        method: 'POST',
        body: JSON.stringify({}),
        headers: {
          'Authorization': `Bearer ${props.accessToken}`,
          'Content-Type': 'application/json',
        },
      }).then((response) => response.json().then((responseJson) => {
        setInputs(responseJson.inputs);
        setUpdateLocation(responseJson.query_callback);
        setIsLoading(false)
      }));
    }
  }, [inputs])

  let body;
  if (isLoading) {
    body = <p>Loading Inputs...</p>
  } else {
    body = (
      <InputsList
        inputs={inputs}
        accessToken={props.accessToken}
        updateLocation={updateLocation}
        setUpdateLocation={setUpdateLocation}
        template={props.template}
      />
    );
  }

  const backToTemplates = () => {
    props.setSelectedTemplate(null);
  }

  return (
    <div>
      <button onClick={backToTemplates}>Back to Templates</button>
      <h1>Inputs for {props.template.name}</h1>
      {body}
    </div>
  )
}
