import {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Login';
import SelectTemplate from './SelectTemplate';
import GeneratePresentation from './GeneratePresentation';

function App() {
  const default_access_token = localStorage.getItem('access_token')
  const [accessToken, setAccessToken] = useState(default_access_token);
  const [accessError, setAccessError] = useState(null);
  const [selectedTemplate, setSelectedTemplate] = useState(null);
  const [isFinishingAuth, setIsFinishingAuth] = useState(false);

  useEffect(() => {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    const code = params.get('code')
    if (code && !accessToken && !accessError && !isFinishingAuth) {
      const state = params.get('state')
      finishAuth(code, state);
      setIsFinishingAuth(true);
    }
  }, [accessToken, accessError, isFinishingAuth]);

  const finishAuth = (code, state) => {
    const data = {
      "client_id": process.env.REACT_APP_MATIK_CLIENT_ID,
      "client_secret": process.env.REACT_APP_MATIK_CLIENT_SECRET,
      "grant_type": "authorization_code",
      "redirect_uri": process.env.REACT_APP_MATIK_REDIRECT_URL,
      "code": code,
      "state": state,
    };
    let formBody = [];
    for (const property in data) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(data[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch(`${process.env.REACT_APP_MATIK_BASE_URL}/api/v1/oauth/token/`, {
      method: "POST",
      body: formBody,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(response => response.json().then((responseJson) => {
      if (response.status === 400 || response.status === 429) {
        setAccessError(JSON.stringify(responseJson))
      } else {
        setAccessToken(responseJson['access_token'])
        localStorage.setItem('access_token', responseJson['access_token']);
      }
    }))
  };

  let body;
  if (!accessToken && !accessError) {
    body = <Login/>;
  } else if (accessError) {
    body = (
      <div>
        <p>There was an error: {accessError}</p>
      </div>
    );
  } else {
    if (selectedTemplate) {
      body = <GeneratePresentation accessToken={accessToken} template={selectedTemplate} setSelectedTemplate={setSelectedTemplate}/>
    } else {
      body = <SelectTemplate setSelectedTemplate={setSelectedTemplate} accessToken={accessToken}/>
    }
  }

  return (
    <main>
      {body}
    </main>
  )
}

export default App;
