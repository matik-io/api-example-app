import {useEffect, useState, useRef} from 'react';

export default function InputsList(props) {
  const [queriesByInputId, setQueriesByInputId] = useState({});
  const [valuesByInputId, setValuesByInputId] = useState({});
  const [isQuerying, setIsQuerying] = useState(false);
  const queryHandle = useRef(null);
  const inputTypeToComponent = {
    string: StringInput,
    list: ListInput,
    date: DateInput,
    date_range: DateRangeInput,
    bool: BoolInput,
  };

  useEffect(() => {
    console.log("HOLA", props.updateLocation)
    if (props.updateLocation) {
      if (queryHandle.current) {
        clearInterval(queryHandle.current);
      }
      const intervalQueryHandle = setInterval(checkForQueryUpdates, 2000);
      queryHandle.current = intervalQueryHandle;
    }
  }, [props.updateLocation])

  const checkForQueryUpdates = () => {
    if (props.updateLocation && !isQuerying) {
      setIsQuerying(true);
      fetch(`${process.env.REACT_APP_MATIK_BASE_URL}/${props.updateLocation}`, {
        headers: {'Authorization': `Bearer ${props.accessToken}`}
      }).then(result => result.json()).then((responseJson) => {
        setQueriesByInputId(Object.assign(queriesByInputId, responseJson.queries_by_input_id));
        console.log(responseJson.input_values_by_id);
        setValuesByInputId(responseJson.input_values_by_id)
        const isDone = Object.keys(responseJson.queries_by_input_id).reduce(
          (isDone, inputId) => isDone && responseJson.queries_by_input_id[inputId].status !== 'processing', true
        );
        console.log('isDone!', isDone, queryHandle.current)
        if (isDone) {
          clearInterval(queryHandle.current);
          queryHandle.current = null;
        }
        setIsQuerying(false);
      });
    }
  }

  const onParamValChange = (e, inputId) => {
    const newVal = e.target.value;
    const updatedValuesByInputId = Object.assign({}, valuesByInputId);
    updatedValuesByInputId[inputId] = newVal;
    setValuesByInputId(updatedValuesByInputId);

    const inputsBody = {
      'input_values_by_id': updatedValuesByInputId,
    }
    if (props.updateLocation) {
      inputsBody['query_callback'] = props.updateLocation;
    }
    props.setUpdateLocation(null);
    fetch(`${process.env.REACT_APP_MATIK_BASE_URL}/api/1.0/templates/${props.template.id}/inputs`, {
      method: 'POST',
      body: JSON.stringify(inputsBody),
      headers: {
        'Authorization': `Bearer ${props.accessToken}`,
        'Content-Type': 'application/json',
      },
    }).then((response) => response.json().then((responseJson) => {
      setIsQuerying(false);
      props.setUpdateLocation(responseJson.query_callback);
    }));
  };

  return (
    <div>
      {props.inputs.map((input) => {
        const InputComponent = inputTypeToComponent[input.parameter_type]
        return (
          <div key={input.id}>
            <p><strong>{input.display_name || input.name}</strong></p>
            <p><small>{input.description}</small></p>
            <div>
              <InputComponent input={input} onParamValChange={onParamValChange} queriesByInputId={queriesByInputId} value={valuesByInputId[input.id]}/>
            </div>
          </div>
        );
      })}
    </div>
  )
}

function StringInput(props) {
  const { input, onParamValChange, queriesByInputId, value } = props;
  if (input.source_type === 'user_input') {
     return <input type="text" value={value} onChange={(e) => onParamValChange(e, input.id)}/>;
  }
  if (input.source_type === 'list') {
    return (
      <select onChange={(e) => onParamValChange(e, input.id)}>
        {input.source_list.map((item) => (<option key={item} value={item}>{item}</option>))}
      </select>
    )
  }

  if (input.source_type === 'query') {
    if (queriesByInputId[input.id]) {
      if (queriesByInputId[input.id].status === 'done') {
        return (
          <select onChange={(e) => onParamValChange(e, input.id)}>
            {queriesByInputId[input.id].options.map((option) => (<option key={option} value={option}>{option}</option>))}
          </select>
        )
      } else {
        return <p>{queriesByInputId[input.id].status}</p>
      }

    } else {
      return (
        <p>ERROR</p>
      )
    }
  }
}

function ListInput(props) {
  const {input, onParamValChange, queriesByInputId, value } = props
  if (input.source_type === 'user_input') {
    return <input type="text" value={value}/>;
  }

  if (input.source_type === 'list') {
    return (
      <select onChange={(e) => onParamValChange(e, input.id)} multiple>
        {input.source_list.map((item) => (<option key={item} value={item}>{item}</option>))}
      </select>
    )
  }

  if (input.source_type === 'query') {
    if (queriesByInputId[input.id]) {
      if (queriesByInputId[input.id].status === 'done') {
        return (
          <select onChange={(e) => onParamValChange(e, input.id)} multiple>
            {queriesByInputId[input.id].options.map((option) => (<option key={option} value={option}>{option}</option>))}
          </select>
        )
      } else {
        return <p>{queriesByInputId[input.id].status}</p>
      }

    } else {
      return (
        <p>ERROR</p>
      )
    }
  }
}

function DateInput(props) {
  const val = props.value || new Date();
  return <input type="date" value={val} onChange={(e) => props.onParamValChange(e, props.param.id)} />
}

function DateRangeInput(props) {

}

function BoolInput(props) {
  return <input type="checkbox" checked={props.value} onChange={(e) => props.onParamValChange(e, props.param.id)} />

}
