import {useEffect, useState} from 'react';

export default function SelectTemplate(props) {
  const [templates, setTemplates] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const onTemplateClick = (e, template) => {
    e.preventDefault();
    props.setSelectedTemplate(template);
  }

  useEffect(() => {
    if (!templates) {
      fetch(`${process.env.REACT_APP_MATIK_BASE_URL}/api/1.0/templates`, {
        headers: {
          'Authorization': `Bearer ${props.accessToken}`
        }
      }).then(response => response.json()).then((responseJson) => {
        setTemplates(responseJson);
        setIsLoading(false);
      })
    }
  }, [templates])
  return (
    <div>
      <h1>Select a Template</h1>
      {isLoading && <p>Loading...</p>}
      {(!templates || templates.length === 0) && !isLoading && <p>No Templates Found</p>}
      {templates && templates.map((template) => (
        <div key={template.id}>
          <a href="#" onClick={e => onTemplateClick(e, template)}>{template.name} - {template.description}</a>
        </div>
      ))}
    </div>
  )
}
