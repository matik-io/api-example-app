import requests
import json
from flask import redirect, request



with open('config.json') as config_file:
    config = json.load(config_file)

CLIENT_ID = config["client_id"]
CLIENT_SECRET = config["client_secret"]
LOCAL_WEBSERVER_URL = config["local_webserver_url"]

# API base URL
MATIK_BASE_URL = "https://localhost"
MATIK_API_BASE_URL = f"{MATIK_BASE_URL}/api/1.0/"
MATIK_API_TOKEN_URL = f"{MATIK_BASE_URL}/api/v1/oauth/token/"
MATIK_AUTH_URL = f"{MATIK_BASE_URL}/oauth/authorize"

def add_authorization(request_headers):
    token = request.cookies.get("token")
    headers = {}
    if token is None:
        if "Authorization" in request_headers:
            headers["Authorization"] = request_headers["Authorization"]
        else:
            raise KeyError('Auth Token Not Found')
    else:
        headers['Authorization'] = f'Bearer {token}'

    return headers


def call_matik_api(path, request_headers=None):
    headers = add_authorization(request_headers)
    return requests.get(MATIK_API_BASE_URL + path, verify=False, headers=headers, allow_redirects=False)


def post_matik_api(path, data=None, json=None, request_headers=None):
    headers = add_authorization(request_headers)
    return requests.post(MATIK_API_BASE_URL + path, data=data, json=json, verify=False, headers=headers)


def put_matik_api(path, data=None, json=None, request_headers=None):
    headers = add_authorization(request_headers)
    return requests.put(MATIK_API_BASE_URL + path, data=data, json=json, verify=False, headers=headers)

def delete_matik_api(path, request_headers=None):
    headers = add_authorization(request_headers)
    return requests.delete(MATIK_API_BASE_URL + path, verify=False, headers=headers)
