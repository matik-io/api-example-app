from flask import Blueprint, request
from helper import call_matik_api, post_matik_api, put_matik_api, delete_matik_api

data_source = Blueprint('data_sources', __name__)

@data_source.route("/",  methods=["GET"])
def data_source_list():
    response = call_matik_api("data_sources", request.headers)
    return response.text


@data_source.route("/<datasource_id>",  methods=["GET"])
def get_data_source(datasource_id):
    response = call_matik_api(f"data_sources/{datasource_id}", request.headers)
    return response.text


@data_source.route("update_connection/<datasource_id>",  methods=["GET"])
def update_oauth_connection(datasource_id):
    response = call_matik_api(f"update_oauth/{datasource_id}", request.headers)
    return response.text


@data_source.route("/",  methods=["POST"])
def create_data_source():
    post_data = request.get_json()
    print(post_data)
    response = post_matik_api("data_sources", json=post_data, request_headers=request.headers)
    return response.text


@data_source.route("/<datasource_id>", methods=["PUT"])
def update_data_source(datasource_id):
    post_data = request.get_json()
    response = put_matik_api(f"data_sources/{datasource_id}", json=post_data, request_headers=request.headers)
    return response.text


@data_source.route("/<datasource_id>",  methods=["DELETE"])
def delete_data_source(datasource_id):
    response = delete_matik_api(f"data_sources/{datasource_id}", request_headers=request.headers)
    return response.text