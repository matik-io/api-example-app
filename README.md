# Api Example App

An example application that showcases Matik's API

## Getting started

 - Install required dependencies using `pip install -r requirements.txt`..
 - Update `config.json` with your client secret/host
 - Run app with `flask run --port=5555`

