from flask import Flask, redirect, request, render_template, jsonify
from oauthlib.oauth2 import WebApplicationClient
from data_source import data_source
from helper import LOCAL_WEBSERVER_URL, CLIENT_ID, CLIENT_SECRET, MATIK_AUTH_URL, MATIK_API_TOKEN_URL, call_matik_api, post_matik_api
import requests
import json


app = Flask(__name__)
app.register_blueprint(data_source, url_prefix="/data_sources")

oauth_redirect_url = f"{LOCAL_WEBSERVER_URL}matik_callback"


@app.route("/")
def hello_world():
    check_url = f"{LOCAL_WEBSERVER_URL}matik_auth"
    return f"""
Visit <a href="{check_url}">here</a> to login        
"""


@app.route("/list_templates")
def list_templates():
    response = call_matik_api("templates")
    return response.text


@app.route("/list_presentations")
def list_presentations():
    response = call_matik_api("presentations")
    return response.text


@app.route("/create_template")
def create_template():
    name = ""
    template_id = 0

    json = {
        "name": name,
        "template_id": template_id,
        "inputs": {}
    }

    # TODO figure out post params and file upload
    response = post_matik_api("templates/", json=json)
    response.raise_for_status()

    if response.status_code == 202:
        template_id = response.headers["location"].split("/")[-1]
        check_url = LOCAL_WEBSERVER_URL + f"check_template_status/{template_id}"
        return f"""
Template {template_id} is generating! Click <a href="{check_url}">here</a> to check generation status        
"""
    else:
        return f"Unexpected {response.status_code} response from Matik"


@app.route("/check_template_status/<template_id>")
def check_template_status(template_id):
    response = call_matik_api(f"templates/queue/{template_id}")
    response.raise_for_status()

    if response.status_code == 303:
        dl_url = LOCAL_WEBSERVER_URL + f"template_details/{template_id}"
        return f"""
Template {template_id} is ready! Click <a href="{dl_url}">here</a> to see details        
"""
    elif response.status_code == 200:
        check_url = LOCAL_WEBSERVER_URL + f"check_template_status/{template_id}"
        return f"""
Template {template_id} NOT ready! Click <a href="{check_url}">here</a> to check again        
"""
    else:
        return f"Unexpected {response.status_code} response from Matik"


@app.route("/template_details/<template_id>")
def template_details(template_id):
    response = call_matik_api(f"templates/{template_id}")
    response.raise_for_status()

    inputs_response = call_matik_api(f"templates/{template_id}/inputs")
    inputs_response.raise_for_status()

    name = response.json()["name"]
    inputs = inputs_response.json()["inputs"]

    input_info = ", ".join(json.dumps(single_input) for single_input in inputs)

    return f"""
name: {name}
inputs: {input_info}     
"""


@app.route("/templates/<template_id>/inputs")
def template_inputs(template_id):
    inputs_response = call_matik_api(f"templates/{template_id}/inputs")
    inputs_response.raise_for_status()

    inputs = inputs_response.json()["inputs"]
    param_options_url = inputs_response.headers.get('X-Param-Options-Location')
    param_options_url = f'/templates/{template_id}/inputs/options?callback={param_options_url}'
    print(param_options_url)

    return jsonify(inputs), 200


@app.route("/templates/<template_id>/inputs/options")
def template_inputs_callback(template_id):
    url = request.args['callback']
    inputs_response = call_matik_api(url)
    inputs_response.raise_for_status()

    return jsonify(inputs_response.json()), 200


@app.route("/create_presentation/<template_id>")
def create_presentation(template_id):
    name = f"creating a new presentation from {template_id}"
    inputs = request.args.to_dict(flat=False)
    for input_name, input_value in inputs.items():
        inputs[input_name] = input_value if len(input_value) > 1 else input_value[0]

    json = {
        "name": name,
        "template_id": template_id,
        "inputs": inputs
    }

    response = post_matik_api("presentations/", json=json)
    response.raise_for_status()

    if response.status_code == 202:
        presentation_id = response.headers["location"].split("/")[-1]
        check_url = LOCAL_WEBSERVER_URL + f"check_presentation_status/{presentation_id}"
        return f"""
Presentation {presentation_id} is generating! Click <a href="{check_url}">here</a> to check generation status        
"""
    else:
        return f"Unexpected {response.status_code} response from Matik"


@app.route("/check_presentation_status/<presentation_id>")
def check_presentation_status(presentation_id):
    response = call_matik_api(f"presentations/queue/{presentation_id}")
    response.raise_for_status()

    if response.status_code == 303:
        dl_url = LOCAL_WEBSERVER_URL + f"download_presentation/{presentation_id}"
        return f"""
Presentation {presentation_id} is ready! Click <a href="{dl_url}">here</a> to download        
"""
    elif response.status_code == 200:
        check_url = LOCAL_WEBSERVER_URL + f"check_presentation_status/{presentation_id}"
        return f"""
Presentation {presentation_id} NOT ready! Click <a href="{check_url}">here</a> to check again        
"""
    else:
        return f"Unexpected {response.status_code} response from Matik"


@app.route("/download_presentation/<presentation_id>")
def download_presentation(presentation_id):
    response = call_matik_api(f"presentations/pdf/{presentation_id}")
    response.raise_for_status()

    download_url = response.json()["download_url"]
    return redirect(download_url, code=302)


########################################################################################################################
# Matik OAuth
########################################################################################################################

@app.route("/matik_auth")
def matik_auth():
    client = WebApplicationClient(CLIENT_ID)

    url = client.prepare_request_uri(
        MATIK_AUTH_URL,
        redirect_uri=oauth_redirect_url,
        scope=['producer', 'consumer'],
        state="asldkajsdlaksjd"
    )

    print('authorization_url', url)

    # Redirect to the complete authorization url
    return redirect(url, code=302)


@app.route("/matik_callback")
def matik_callback():
    client = WebApplicationClient(CLIENT_ID)

    code = request.args['code']
    state = request.args['state']

    data = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "authorization_code",
        "redirect_uri": oauth_redirect_url,
        "code": code,
        "state": state,
    }

    response = requests.post(MATIK_API_TOKEN_URL, data=data, verify=False)
    print(response.text)
    client.parse_request_body_response(response.text)

    token = client.token['access_token']

    response = redirect(LOCAL_WEBSERVER_URL + 'list_templates', code=302)
    response.set_cookie('token', token)

    return response

